/* [Plug parameters] */

// The portion of the plug that fits into the hole.
plug_diameter = 17.8; // [0:50]

// The amount of overhang on the outside of the plug.
plug_overhang = 3;  // [0:10]

// Should the plug be solid or hollow?
plug_is_hollow = true;

/* [Hidden] */
plug_overhang_thickness = 2;
plug_inner_thickness = 4;

difference()
{
  union()
  {
    cylinder(d=plug_diameter + 2*plug_overhang, h=plug_overhang_thickness);
    translate([0, 0, plug_overhang_thickness])
    {
      cylinder(d=plug_diameter, h=plug_inner_thickness);
    }
  }

  if (plug_is_hollow)
  {
    translate([0, 0, -3])
    {
      cylinder(d=plug_diameter - 4, h=plug_overhang_thickness + plug_inner_thickness);
    }
  }
}
